#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sublime, sublime_plugin
from time import sleep
import re 

class dateCommand(sublime_plugin.TextCommand):

	def run(self, edit):

		d1 = ["сто", "ста", "ста", "сотого",
    		"двести", "двухсот", "двумястами", "двухсотого",
    		"триcта", "трехсот", "тремястами", "трехсотого",
    		"четыреста", "четырьмястами", "четырехсот", "четырехсотого",
    		"пятьсот", "пятьюстами", "пятисот", "пятисотого",
    		"шестьсот", "шестисот", "шестьюстами", "шестисотого",
    		"семьсот", "семисот", "семьюстами", "семисотого",
    		"восемьсот", "восьмисот", "восмьюстами", "восьмисотого",
    		"девятьсот", "девятисот", "девятьюстами", "девятисотого"]

		k1 = ["первое", "первому", "первого", "первый", "первом", "первым", 
    		"второе", "второму", "второго", "второй", "втором", "вторым",
    		"третье", "третьему", "третьего", "третий", "третьем", "третьим",
    		"четвертое", "четвертому", "четвертого", "четвертый", "четвертом", "четвертым",
    		"пятое", "пятому", "пятого", "пятый", "пятом", "пятым",
    		"шестое", "шестому", "шестого", "шестой", "шестом", "шестым",
    		"седьмое", "седьмому", "седьмого", "седьмой", "седьмом", "седьмым",
    		"восьмое", "восьмому", "восьмого", "восьмой", "восьмом", "восьмым",
    		"девятое", "девятому", "девятого", "девятый", "девятом", "девятым"]


		t1 = [
			"одиннадцатое", "одиннадцатого", "одиннадцатому", "одиннадцатым", "одиннадцатом",
    		"двенадцатое", "двенадцатого", "двенадцатому", "двенадцатым", "двенадцатом",
    		"тринадцатое", "тринадцатого", "тринадцатому", "тринадцатым", "тринадцатом",
    		"четырнадцатое", "четырнадцатого", "четырнадцатому", "четырнадцатым", "четырнадцатом",
    		"пятнадцатое", "пятнадцатого", "пятнадцатому", "пятнадцатым", "пятнадцатом",
    		"шестнадцатое", "шестнадцатого", "шестнадцатому", "шестнадцатым", "шестнадцатом",
    		"семнадцатое", "семнадцатого", "семнадцатому", "семнадцатым", "семнадцатом",
    		"восемнадцатое", "восемнадцатого", "восемнадцатому", "восемнадцатым", "восемнадцатом",
    		"девятнадцатое", "девятнадцатого", "девятнадцатому", "девятнадцатым", "девятнадцатом"]

		m1 = ["десятое", "десятого", "десятому", "десять", "десятом", "десятым",
    		"двадцатое", "двадцатого", "двадцатому", "двадцать", "двадцатом", "двадцатым",
    		"тридцатое", "тридцатого", "тридцатому", "тридцать", "тридцатом", "тридцатым",
    		"сороковое", "сорокового", "сороковому", "сорок", "сороковом", "сороковым",
    		"пятидесятое", "пятидесятого", "пятидесятому",  "пятьдесят", "пятидесятом", "пятидесятым",
    		"шестидесятое", "шестидесятого", "шестидесятому", "шестьдесят", "шестидесятом", "шестидесятым",
    		"семидесятое", "семидесятого", "семидесятому", "семьдесят", "семидесятом", "семидесятым",
    		"восьмидесятое", "восьмидесятого", "восьмидесятому",  "восемьдесят", "восмьидесятом", "восьмидесятым",
    		"девяностое", "девяностого", "девяностому", "девяносто", "девяностом", "девяностым"]

		s1 = ["тысячный", "двух", "трех", "четырех", "пяти", "шести", "семи", "восьми", "девяти"]

		g1 = ["тысяча", "две тысячи", "три тысячи", "четыре тысячи", "пять тысяч", "шесть тысяч", "семь тысяч", "восемь тысяч", "девять тысяч"]

		yt = ["тысячи", "тысяч", "тысячный"]
		montht = ["", "январем", "февралем", "мартом", "апрелем", "маем", "июнем", "июлем",
			"августом", "сентябрем", "октябрем", "ноябрем", "декабрем"]
		monthd = ["", "январю", "февралю", "марту", "апрелю", "маю", "июню", "июлю", 
			"августу", "сентябрю", "октябрю", "ноябрю", "декабрю"]
		monthp = ["", "январе", "феврале", "марте", "апреле", "мае", "июне", "июле",
			"августе", "сентябре", "октябре", "ноябре", "декабре"]
		month = ["", "января", "февраля", "марта", "апреля", "мая", "июня", "июля",
         	"августа", "сентября", "октября", "ноября", "декабря"]

		bigarr = d1 + k1 + month + t1 + m1 + s1 + g1 + yt



		def test(a):
			return a[0]

		def convert2(a, p):
			answer = ""
			global month2
			if p == "k":
				month2 = month
				var3 = int(a[1])*5 - 5
				var1 = int(a[1])*6 - 6
				var2 = int(a[0])*6 - 6
			if p == "d":
				month2 = monthd
				var3 = int(a[1])*5 - 3
				var1 = int(a[1])*6 - 5
				var2 = int(a[0])*6 - 4
				var4 = int(a[1])*6 - 3
			if p == "i":
				month2 = month
				var3 = int(a[1])*5 - 4
				var1 = int(a[1])*6 - 4
				var2 = int(a[0])*6 - 5
				var4 = int(a[1])*4 - 3
			if p == "p":
				month2 = monthp
				var3 = int(a[1])*5 - 1
				var1 = int(a[1])*6 - 2
				var2 = int(a[0])*6 - 2
				var4 = int(a[1])*4 - 3
			if p == "t":
				month2 = montht
				var3 = int(a[1])*5 - 2
				var1 = int(a[1])*6 - 1
				var2 = int(a[0])*6 - 1
				var4 = int(a[1])*4 - 3

			if a[0] == "1":
				if a[1] == "0":
					if p == "p":
						answer = answer + "десятом" + " "
					if p == "i":
						answer = answer + "десятого" + " "
					if p == "k":
						answer = answer + "десятое" + " "
					if p == "d":
						answer = answer + "десятому" + " "
					if p == "t":
						answer = answer + "десятым" + " "
				else:
					answer = answer + t1[var3] + " "
			else:
				if a[0]=="0":
					 answer = answer + k1[var1] + " "
				else:
					if a[1]=="0":
						answer = answer + m1[var2] + " "
					else:
						answer = answer + m1[int(a[0])*6 - 3] + " " + k1[var1] + " "
			if a[3] == "0":
				answer = answer + month2[int(a[4])] + " "
			else:
				answer = answer + month2[10 + int(a[4])] + " "
			if len(a) == 5:
				return answer
			if len(a)<10:
				return answer
			if a[6] != "0" and a[7]=="0" and a[8] == "0" and a[9] == "0":
				if a[6] == "1":
					answer = answer + "тысячного"
				else:
					answer = answer + s1[int(a[6]) - 1] + "тысячного"
			if a[6]!="0" and a[7]!="0" and a[8]=="0" and a[9]=="0":
				answer = answer + g1[int(a[6])-1] + " " + d1[int(a[7])*4 - 1]
			if a[6]!="0" and a[7]!="0" and a[8]!="0" and a[9]=="0":
				answer = answer + g1[int(a[6])-1] + " " + d1[int(a[7])*4 - 4] + " " + m1[int(a[8])*6 - 5]
			if a[6]!="0" and a[7]!="0" and a[8]!="0" and a[9]!="0":
				if a[8] == "1":
					answer = answer + g1[int(a[6])-1] + " " + d1[int(a[7])*4 - 4] + " " + t1[int(a[9])*5 - 4]
				else:
					answer = answer + g1[int(a[6])-1] + " " + d1[int(a[7])*4 - 4] + " " + m1[int(a[8])*6 - 3] + " " + k1[int(a[9])*6 - 4]
			if a[6]!="0" and a[7]=="0" and a[8]=="0" and a[9]!="0":
				answer = answer + g1[int(a[6])-1] + " " + k1[int(a[9])*6 - 4]
			if a[6]!="0" and a[7]=="0" and a[8]!="0" and a[9]!="0":
				if a[8]=="1":
					answer = answer + g1[int(a[6])-1] + " " + t1[int(a[9])*5 - 4]
				else:
					answer = answer + g1[int(a[6])-1] + " " + m1[int(a[8])*5 - 1] + " " + k1[int(a[9])*6 - 4]
			if a[6]!="0" and a[7]!="0" and a[8]=="0" and a[9]!="0":
				answer = answer + g1[int(a[6])-1] + " " + d1[int(a[7])*4 - 4] + " " + k1[int(a[9])*6 - 4]
			if a[6]!="0" and a[7]=="0" and a[8]!="0" and a[9]=="0":
				answer = answer + g1[int(a[6])-1] + " " + m1[int(a[8])*6 - 5]
			return answer + " года"

		def convert1(a, p):
			global month1
			if p == "i":
				month1 = month
			if p == "d":
				month1 = monthd
			if p == "t":
				month1 = montht
			if p == "p":
				month1 = monthp
			if a[0] == '0':
				answer = str(a[1:2]) + " "
			else:
				answer = str(a[0:2]) + " "
			if a[3]=='0':
				answer = answer + month1[int(a[4])] + " "
			else:
				answer = answer + month1[int(a[3:5])] + " "
			if len(a) == 10:
				return answer + str(a[len(a)-4:])
			else:
				return answer[:len(answer)-1]

		def detp(a):
			var = 0
			for i in range(1, len(a)):
				if a[i] == " ":
					if a[var:i] in montht:
						return "t"
					if a[var:i] in monthd:
						return "d"
					if "ое" in a[var:i] or "ье " in a[var:i]:
						return "k"
					if a[var:i] in month:
						return "i"
					if a[var:i] in monthp:
						return "p"
					var = i + 1
				if i == len(a)-1:
					if a[var:] in montht:
						return "t"
					if a[var:] in monthd:
						return "d"
					if a[var:] in month:
						return "i"
					if a[var:] in monthp:
						return "p"
					var = i + 1
		

		def isanumber(a):
			numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
			for number in numbers:
				if a == number: 
					return True
			return False

		def isadate(a):
			if len(a) == 10:
				if (isanumber(a[0]) == False or isanumber(a[1]) == False or a[2] != "." or isanumber(a[3]) == False or isanumber(a[4]) == False or a[5] != "." or isanumber(a[6]) == False or 
				isanumber(a[7]) == False or isanumber(a[8]) == False or isanumber(a[9]) == False):
					return False
				else:
					return True
			else:
				return False

		def convs(a):
			if a[1] == " " :
				a = '0' + a
			if isadate(a):
				return a
			if (isanumber(a[len(a)-1])) == False:
				str = a[3:len(a)]
				if str == "декабря" or str == "декабрю" or str == "декабре" or str == "декабрем":
					return a[0:2] + ".12" 
				if str == "января" or str == "январю" or str == "январем"  or str == "январе":
					return a[0:2] + ".01" 
				if str == "февраля" or str == "февралю" or str == "февралем" or str == "феврале":
					return a[0:2] + ".02" 
				if str == "марта" or str == "марту" or str == "мартом" or str == "марте":
					return a[0:2] + ".03" 
				if str == "апреля" or str == "апрелю" or str == "апрелем" or str == "апреле":
					return a[0:2] + ".04" 
				if str == "мая" or str == "маю" or str == "маем" or str == "мае":
					return a[0:2] + ".05" 
				if str == "июня" or str == "июню" or str == "июнем" or str == "июне":
					return a[0:2] + ".06" 
				if str == "июля" or str == "июлю" or str == "июлем" or str == "июле":
					return a[0:2] + ".07" 
				if str == "августа"  or str == "августу" or str == "августом" or str == "августе":
					return a[0:2] + ".08" 
				if str == "сентября" or str == "сентябрю" or str == "сентябрем" or str == "сентябре":
					return a[0:2] + ".09" 
				if str == "октября" or str == "октябрю" or str == "октябрем" or str == "октябре":
					return a[0:2] + ".11" 
				if str == "ноября" or str == "ноябрю" or str == "ноябрем" or str == "ноябре":
					return a[0:2] + ".11" 
			else:
				str = a[3:len(a)-5]
				if str == "декабря" or str == "декабрю" or str == "декабре" or str == "декабрем":
					return a[0:2] + ".12." + a[len(a)-4:]
				if str == "января" or str == "январю" or str == "январем"  or str == "январе":
					return a[0:2] + ".01." + a[len(a)-4:]
				if str == "февраля" or str == "февралю" or str == "февралем" or str == "феврале":
					return a[0:2] + ".02." + a[len(a)-4:]
				if str == "марта" or str == "марту" or str == "мартом" or str == "марте":
					return a[0:2] + ".03." + a[len(a)-4:]
				if str == "апреля" or str == "апрелю" or str == "апрелем" or str == "апреле":
					return a[0:2] + ".04." + a[len(a)-4:]
				if str == "мая" or str == "маю" or str == "маем" or str == "мае":
					return a[0:2] + ".05." + a[len(a)-4:]
				if str == "июня" or str == "июню" or str == "июнем" or str == "июне":
					return a[0:2] + ".06." + a[len(a)-4:]
				if str == "июля" or str == "июлю" or str == "июлем" or str == "июле":
					return a[0:2] + ".07." + a[len(a)-4:]
				if str == "августа"  or str == "августу" or str == "августом" or str == "августе":
					return a[0:2] + ".08." + a[len(a)-4:]
				if str == "сентября" or str == "сентябрю" or str == "сентябрем" or str == "сентябре":
					return a[0:2] + ".09." + a[len(a)-4:]
				if str == "октября" or str == "октябрю" or str == "октябрем" or str == "октябре":
					return a[0:2] + ".10." + a[len(a)-4:]
				if str == "ноября" or str == "ноябрю" or str == "ноябрем" or str == "ноябре":
					return a[0:2] + ".11." + a[len(a)-4:]
			
		
		global dt
		dt = {}
		def captst(a):
			global newdate
			newdate = a
			var = 0
			var1 = 0
			var2 = 0
			if isadate(a):
				for i in range(0, len(text)):
					if text[i:i+10] == dat:
						v.replace(edit, sublime.Region(i, i + 10), newdate)
			if dt!={}:
				ds = dt[dat]
			#v.insert(edit,0, text[0:0+len(convert2(dat, ds[var2]).decode('utf-8'))-1] + " " + convert2(dat, ds[var2]).decode('utf-8') + " ")
			#v.insert(edit, 0, str(text[56:56+len(convert1(dat, ds[1]).decode('utf-8'))] ==  convert1(dat, ds[1]).decode('utf-8') ))
			
			for i in range(0, len(text)):
				if text[i:i+len(convert1(dat, ds[var1]).decode('utf-8'))] == convert1(dat, ds[var1]).decode('utf-8'):
					#v.insert(edit, 0,"HEH")
					v.replace(edit, sublime.Region(var, var + len(convert1(dat, ds[var1]).decode('utf-8'))), convert1(newdate, ds[var1]).decode('utf-8'))
					var2 = var
					if len(text[i:i+len(convert1(dat, ds[var1]).decode('utf-8'))]) < len(convert1(newdate, ds[var1]).decode('utf-8')):
						var = var + len(convert1(newdate, ds[var1]).decode('utf-8')) - len(text[i:i+len(convert1(dat, ds[var1]).decode('utf-8'))])
					if len(text[i:i+len(convert1(dat, ds[var1]).decode('utf-8'))]) > len(convert1(newdate, ds[var1]).decode('utf-8')):
						var = var - len(convert1(dat, ds[var1]).decode('utf-8')) + len(text[i:i+len(convert1(newdate, ds[var1]).decode('utf-8'))]) 
					var1 = var1+1
				if text[i:i+len(convert2(dat, ds[var1]).decode('utf-8'))] == convert2(dat, ds[var1]).decode('utf-8'):
					v.replace(edit, sublime.Region(var, var + len(text[i:i+len(convert2(dat, ds[var1]).decode('utf-8'))])), convert2(newdate, ds[var1]).decode('utf-8'))
					var2 = var
					if len(text[i:i+len(convert2(dat, ds[var1]).decode('utf-8'))]) < len(convert2(newdate, ds[var1]).decode('utf-8')):
						var = var + len(convert2(newdate, ds[var1]).decode('utf-8')) - len(text[i:i+len(convert2(dat, ds[var1]).decode('utf-8'))])
					if len(text[i:i+len(convert2(dat, ds[var1]).decode('utf-8'))]) > len(convert2(newdate, ds[var1]).decode('utf-8')):
						var = var - len(convert2(dat, ds[var1]).decode('utf-8')) + len(text[i:i+len(convert2(newdate, ds[var1]).decode('utf-8'))])
					var1 = var1+1
				var = var + 1

			
				#for i in range(0, len(text)):
					#if text[i:i+len(convert2(dat, ds[var2]).decode('utf-8'))] == convert2(dat, ds[var2]).decode('utf-8'):
						#v.insert(edit, 0,"HEH")
						#v.replace(edit, sublime.Region(var, var + len(text[i:i+len(convert2(dat, ds[var2]).decode('utf-8'))])), convert2(newdate, ds[var2]).decode('utf-8'))
						#var1 = var2
						#var2 = var2+1
						#if len(text[i:i+len(convert2(dat, ds[var1]).decode('utf-8'))]) < len(convert2(newdate, ds[var1]).decode('utf-8')):
						#	var = var + len(convert2(newdate, ds[var1]).decode('utf-8')) - len(text[i:i+len(convert1(dat, ds[var1]).decode('utf-8'))])
						#if len(text[i:i+len(convert2(dat, ds[var1]).decode('utf-8'))]) > len(convert2(newdate, ds[var1]).decode('utf-8')):
						#	var = var - len(convert2(dat, ds[var1]).decode('utf-8')) + len(text[i:i+len(convert1(newdate, ds[var1]).decode('utf-8'))]) 
					#var = var + 1



		def selected(a):
			global dat
			if date!=[]:
				dat = date[a]
				w.show_input_panel(date[a] + "->", "", captst, None, None)

		global rek
		rek = ["", re.compile('январ(ь|я|е|ю|ем)'), re.compile('феврал(ь|я|е|ю|ём)'), re.compile('март(а|у|е|ом)'), re.compile('апрел(ь|я|е|ю|ем)'), re.compile('ма(й|я|е|ю|ем)'), re.compile('июн(ь|я|е|ю|ем)'), 
				re.compile('июл(ь|я|е|ю|ем)'), re.compile('август(а|е|у|ом)'), re.compile('сентябр(ь|я|е|ю|ём)'), re.compile('октябр(ь|я|е|ю|ём)'), re.compile('ноябр(ь|я|е|ю|ём)'), re.compile('декабр(ь|я|е|ю|ём)')]
		def to_num(date):
			dayn = 0
			yearn = 0
			num = 0
			for i in range(1,13):
				if rek[i].search(date)!=None:
					monthn = month[i]
					num = i
			begin = rek[num].search(date).start()
			end = rek[num].search(date).end()
			day = date[:begin-1]
			year = date[rek[num].search(date).end()+1:]
			if "двадцат" in day:
				dayn = dayn + 20
			if "тридцат" in day:
				dayn = dayn + 30
			if "первое" in day or  "первому" in day or "первом" in day or  "первым" in day  or  "первого" in day:
				dayn = dayn + 1
			if "второе" in day or  "второму" in day or  "втором" in day or  "вторым" in day or  "второго" in day :
				dayn = dayn + 2
			if "третье" in day or  "третьему" in day or  "третьем" in day or  "третьим" in day or  "третьего" in day:
				dayn = dayn + 3
			if "четвертое" in day or  "четвертому" in day or  "четвертом" in day or  "четвертым" in day  or  "четвертого" in day:
				dayn = dayn + 4
			if "пятое" in day or  "пятому" in day or  "пятом" in day or  "пятым" in day or  "пятого" in day :
				dayn = dayn + 5
			if "шестое" in day or  "шестому" in day or  "шестом" in day or  "шестым" in day  or  "шестого" in day:
				dayn = dayn + 6
			if "седьмое" in day or  "седьмому" in day or  "седьмом" in day or  "седьмым" in day  or  "седьмого" in day:
				dayn = dayn + 7
			if "восьмое" in day or  "восьмому" in day or  "восьмом" in day or  "восьмым" in day  or  "восьмого" in day:
				dayn = dayn + 8
			if "девятое" in day or  "девятому" in day or  "девятом" in day or  "девятым" in day  or  "девятого" in day:
				dayn = dayn + 9
			if "десятое" in day or  "десятому" in day or  "десятом" in day or  "десятым" in day  or  "десятого" in day:
				dayn = dayn + 10
			if "одиннадцат" in day:
				dayn = dayn + 11
			if "двенадцат" in day:
				dayn = dayn + 12
			if "тринадцат" in day:
				dayn = dayn + 13
			if "четырнадцат" in day:
				dayn = dayn + 14
			if "пятнадцат" in day:
				dayn = dayn + 15
			if "шестнадцат" in day:
				dayn = dayn + 16
			if "восемнадцат" in day:
				dayn = dayn + 18
			else:
				if "семнадцат" in day:
					dayn = dayn + 17
			if "девятнадцат" in day:
				dayn = dayn + 19
			if "две тысячи" in year:
				yearn = yearn + 2000
			if "двухтысячного" in year:
				yearn = yearn + 2000
			if "пять тысяч" in year:
				yearn = yearn + 5000
			if "три тысячи" in year:
				yearn = yearn + 3000
			if "четыре тысячи" in year:
				yearn = yearn + 4000
			if "тысяча" in year or "тысячного" in year:
				yearn = yearn + 1000
			if "перв" in year:
				yearn = yearn + 1
			if "второе" in year or "второй" in year or "второго" in year or "втором" in year or "второму" in year :
				yearn = yearn + 2
			if "третье" in year or "третий" in year or "третьего" in year or "третим" in year or "третьему" in year :
				yearn = yearn + 3
			if "четвертое" in year or "четвертый" in year or "четвертого" in year or "четвертым" in year or "четвертому" in year  :
				yearn = yearn + 4
			if "пятое" in year or "пятый" in year or "пятого" in year or "пятым" in year or "пятому" in year  :	
				yearn = yearn + 5
			if "шестое" in year or "шестой" in year or "шестого" in year or "шестым" in year or "шестому" in year :
				yearn = yearn + 6
			if "седьмое" in year or "седьмой" in year or "седьмого" in year or "седьмым" in year or "седьмому" in year  :
				yearn = yearn + 7
			if "восьмое" in year or "восьмое" in year or "восьмого" in year or "восьмым" in year or "восьмому" in year :
				yearn = yearn + 8
			if "девятое" in year or "девятый" in year or "девятого" in year or "девятым" in year or "девятому" in year :
				yearn = yearn + 9
			if " десятое" in year or " десятый" in year or " десятого" in year or " десятым" in year or " десятому" in year :
				yearn = yearn + 10
			if " сто" in year or " сот" in year:
				yearn = yearn + 100
			if "двести" in year or "двухсот" in year:
				yearn = yearn + 200
			if "триста" in year or "трехсот" in year:
				yearn = yearn + 300
			if "четыреста" in year or "четырехсот" in year:
				yearn = yearn + 400
			if "пятьсот" in year or "пятисот" in year:
				yearn = yearn + 500
			if "шестьсот" in year or "шестисот" in year:
				yearn = yearn + 600
			if "восемьсот" in year or "восьмисот" in year:
				yearn = yearn + 800
			else:
				if "семьсот" in year or "семисот" in year:
					yearn = yearn + 700
			if "девятьсот" in year or "девятисот" in year:
				yearn = yearn + 900
			if "двадцат" in year:
				yearn = yearn + 20
			if "тридцат" in year:
				yearn = yearn + 30
			if "сорок" in year:
				yearn = yearn + 40
			if "пятьдесят" in year or "пятидесят" in year:
				yearn = yearn + 50
			if "шестьдесят" in year or "шестидесят" in year:
				yearn = yearn + 60
			if "восемьдесят" in year or "восмьидесят" in year:
				yearn = yearn + 80
			else:
				if "семьдесят" in year or "семидесят" in year:
					yearn = yearn + 70
			if "девяност" in year:
				yearn = yearn + 90
			if "одиннадцат" in year:
				yearn = yearn + 11
			if "двенадцат" in year:
				yearn = yearn + 12
			if "тринадцат" in year:
				yearn = yearn + 13
			if "четырнадцат" in year:
				yearn = yearn + 14
			if "пятнадцат" in year:
				yearn = yearn + 15
			if "шестнадцат" in year:
				yearn = yearn + 16
			if "семнадцат" in year:
				yearn = yearn + 17
			if "девятьнадцат" in year:
				yearn = yearn + 19
			if num < 10:
				num = "0" + str(num)
			else:
				num = str(num)

			if len(str(dayn) + "." + num + "." + str(yearn)) == 9:
				return "0" + str(dayn) + "." + num + "." + str(yearn)
			else:
				return str(dayn) + "." + num + "." + str(yearn)

		v = self.view

		w = v.window()


		v.set_encoding('utf-8')


		name = v.file_name()

		intput = open(v.file_name(), "r+")

		global text
		text1 = intput.read()
		text = text1.decode('utf-8')

		date = []
		origin = []

		for i in range(0, len(text)):
			
			if isadate(text[i:i+10]):
				if (text[i:i+10] in date) == False:
					date.append(str(text[i:i+10]))

		global p
		p = re.compile('(31|30|1?[0-9]|2?[0-9]){1} (январ(ь|ю|я|ем|е)|феврал(ь|ю|я|ем|е)|сентябр(ь|ю|я|ем|е)|октябр(ь|ю|я|ем|е)|ноябр(ь|ю|я|ем|е)|декабр(я|ь|ю|ем|е)|март(у|а|ом|е)|август(у|а|ом|е)|апрел(ь|ю|я|ем|е)|июн(ь|ю|я|ем|е)|июл(ь|ю|я|ем|е)|ма(й|ю|я|ем|е)){1}( \d{4})?')
		k = re.compile('((двадцать |тридцать )?((перв|втор|четверт|пят|шест|седьм|восьм|девят|десят)(ое|ого|ому|ом|ым)|треть(ем|его|ему|им|е))|(один|две|три|четыр|пят|шест|сем|восем|девят)надцат(ое|ого|ому|ом|ым)|(двадцат|тридцат)(ое|ого|ому|ом|ым)){1} ((январ|феврал|сентябр|октябр|ноябр|декабр)(ь|я|ем|ю|е)|(март|август)(а|е|у|ом)?|(апрел|июн|июл)(ь|я|ем|ю|е)|ма(я|ю|й|ем|е)){1}( (((тысяча |(две|три|четыре) тысячи )?((сто |двести |(три|четыре)ста |(шесть|семь|восемь|девять)сот )?(первого|второго|третьего|чётвёртого|пятого|шестого|седьмого|восьмого|девятого|(один|две|три|четыр|пят|шест|сем|восем|девят)надцатого|(двадцать|тридцать|сорок|(шесть|семь|восемь)десят|девяносто) (первого|второго|третьего|четвёртого|пятого|шестого|седьмого|восьмого|девятого)|десятого|двадцатого|тридцатого|сорокового|(пяти|шести|семи|восьми)десятого|девяностого))|(двух|трёх|четырёх|пяти|шести|семи|восьми|девяти)?сотого))| двухтысячного| тысячного)?')

		
		end = 0
		text2 = text
		flag = True
	#	while flag == True:
		#	if p.search(text1[end:])!=None:
			#	sdate = p.search(text1[end:]).group()
				#v.insert(edit, 0, p.search(text1[end:]).group().decode('utf-8') + " " + detp(sdate))
				#if (convs(sdate) in date) == False:
				#	date.append(convs(sdate))
				#	pad = detp(sdate)
				#ds.append(detp(sdate))
				#end = p.search(text1[end:]).end() + end
			#else:
				#flag = False

		end = 0
		flag = True
		#while flag == True:
			#if k.search(text1[end:])!=None:
			#	sdate = k.search(text1[end:]).group()
				#v.insert(edit, 0, k.search(text1[end:]).group().decode('utf-8'))
				#if (to_num(sdate) in date) == False:
				#	date.append(to_num(sdate))
				
				#ds.append(detp(sdate))
				#end = k.search(text1[end:]).end() + end
			#else:
				#flag = False
		end =0
		while flag == True:
			if p.search(text1[end:])!=None and k.search(text1[end:])!=None:
				if p.search(text1[end:]).end() < k.search(text1[end:]).end():
					#v.insert(edit, 0, p.search(text1[end:]).group().decode('utf-8'))
					if (convs(p.search(text1[end:]).group()) in date) == False:
						date.append(convs(p.search(text1[end:]).group()))
					if (convs(p.search(text1[end:]).group()) in dt.keys())==False:
						dt[convs(p.search(text1[end:]).group())] = []
					dt[convs(p.search(text1[end:]).group())].append(detp(p.search(text1[end:]).group()))
					end = p.search(text1[end:]).end() + end
				else:
					#v.insert(edit, 0, k.search(text1[end:]).group().decode('utf-8'))
					if (to_num(k.search(text1[end:]).group()) in date) == False:
						date.append(to_num(k.search(text1[end:]).group()))
					if (to_num(k.search(text1[end:]).group()) in dt.keys())==False:
						dt[to_num(k.search(text1[end:]).group())] = []
					dt[to_num(k.search(text1[end:]).group())].append(detp(k.search(text1[end:]).group()))
					end = k.search(text1[end:]).end() + end
			else:
				if p.search(text1[end:])!=None and k.search(text1[end:])==None:
					#v.insert(edit,0,p.search(text1[end:]).group().decode('utf-8') + "+")
					if (convs(p.search(text1[end:]).group()) in date) == False:
						date.append(convs(p.search(text1[end:]).group()))
					if (convs(p.search(text1[end:]).group()) in dt.keys())==False:
						dt[convs(p.search(text1[end:]).group())] = []
					dt[convs(p.search(text1[end:]).group())].append(detp(p.search(text1[end:]).group()))
					end = p.search(text1[end:]).end() + end
				else:
					if p.search(text1[end:])==None and k.search(text1[end:])!=None:
						#v.insert(edit,0,k.search(text1[end:]).group().decode('utf-8') + " ")
						if (to_num(k.search(text1[end:]).group()) in date) == False:
							date.append(to_num(k.search(text1[end:]).group()))
						if (to_num(k.search(text1[end:]).group()) in dt.keys())==False:
							dt[to_num(k.search(text1[end:]).group())] = []
						dt[to_num(k.search(text1[end:]).group())].append(detp(k.search(text1[end:]).group()))
						end = k.search(text1[end:]).end() + end
					else:
						flag = False

		#ds[0] = "d"

		#v.insert(edit, 0, str(dt["03.03.2003"]))
		#v.replace(edit, sublime.Region(0,53), convert2("16.01.2016", "d").decode('utf-8'))
		#v.insert(edit, 0, convert2("26.03.2015", "t").decode('utf-8'))
		
		w.show_quick_panel(date, selected)
		
		

		#v.replace(edit, v.sel()[0], hex(int(dec))[2:].upper())

		#name = str(v.file_name())

		#v.insert(edit, 0, v.file_name()) 

		#{ "keys": ["ctrl+shift+h"], "command": "mem" }